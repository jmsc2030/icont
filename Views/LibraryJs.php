
<!-- javascripts -->
<script src="<?php echo $urlViews; ?>js/jquery.js"></script>
<script src="<?php echo $urlViews; ?>js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo $urlViews; ?>js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo $urlViews; ?>js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="<?php echo $urlViews; ?>js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="<?php echo $urlViews; ?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo $urlViews; ?>js/jquery.nicescroll.js" type="text/javascript"></script>
<!-- charts scripts -->

<script src="<?php echo $urlViews; ?>js/jquery.sparkline.js" type="text/javascript"></script>
<script src="<?php echo $urlViews; ?>js/owl.carousel.js"></script>

<!--script for this page only-->
<!-----------------------------------<script src="js/calendar-custom.js"></script>----->
<script src="<?php echo $urlViews; ?>js/jquery.rateit.min.js"></script>
<!-- custom select -->
<script src="<?php echo $urlViews; ?>js/jquery.customSelect.min.js"></script>


<!--custome script for all page-->
<script src="<?php echo $urlViews; ?>js/scripts.js"></script>
<!-- custom script for this page-->
<script src="<?php echo $urlViews; ?>js/sparkline-chart.js"></script>
<script src="<?php echo $urlViews; ?>js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo $urlViews; ?>js/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo $urlViews; ?>js/xcharts.min.js"></script>
<script src="<?php echo $urlViews; ?>js/jquery.autosize.min.js"></script>
<script src="<?php echo $urlViews; ?>js/jquery.placeholder.min.js"></script>
<script src="<?php echo $urlViews; ?>js/gdp-data.js"></script>
<script src="<?php echo $urlViews; ?>js/morris.min.js"></script>
<script src="<?php echo $urlViews; ?>js/sparklines.js"></script>
<script src="<?php echo $urlViews; ?>js/charts.js"></script>
<script src="<?php echo $urlViews; ?>js/jquery.slimscroll.min.js"></script>
<script src="<?php echo $urlViews; ?>js/zabuto_calendar.js"></script>
<script src="<?php echo $urlViews; ?>js/ajax.js"></script>
<script language="JavaScript" type="text/javascript"src="<?php echo $urlViews; ?>js/ajaxPos.js" ></script>



<script type="application/javascript">
    $(document).ready(function () {
        $("#my-calendar").zabuto_calendar({
            language: "es",
            today: true,
            nav_icon: {
                prev: '<i class="fa fa-chevron-circle-left"></i>',
                next: '<i class="fa fa-chevron-circle-right"></i>'
            }
        });
    });
</script>

<!-- DataTables JavaScript -->
<script src="<?php echo $urlViews; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo $urlViews; ?>js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>


<!--<script src="--><?php //echo $urlViews; ?><!--/js/print/jquery-1.4.4.min.js" type="text/javascript"></script>-->
<script src="<?php echo $urlViews; ?>/js/print/jquery.printPage.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $(".btnPrint").printPage();
    });
</script>
<script>
    function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();
 
        var files = evt.dataTransfer.files; // FileList object.
        // files is a FileList of File objects. List some properties.
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
 
            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['Nombre: ', escape(theFile.name), ' || Tamanio: ', escape(theFile.size), ' bytes || type: ', escape(theFile.type), '<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"style="width:50%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);
 
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
        document.getElementById('list-datos').innerHTML = '<ul>' + output.join('') + '</ul>';
    }
 
    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }
 
    // Setup the dnd listeners.
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);
</script>
 
<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object
 
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
 
            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
           }
 
            var reader = new FileReader();
 
            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '" style="width:50%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);
 
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>  