<?php

class Conexion{
    private $user;
    private $password;
    private $server;
    private  $database;
    private $con;

    public function __construct(){
      $user= 'root';
      $password= '';
      $server= 'localhost';
      $database= 'codesanpos';
      $this->con= new mysqli($server,$user,$password,$database);
    }

     public function getUser($usuario,$password){
     $query = $this->con->query("SELECT * FROM usuarios WHERE login='" . $usuario . "' AND password='" . $password . "'");
       $retorno = [];
        $i=0;
        while ($fila = $query->fetch_assoc()){
         $retorno[$i] = $fila;
         $i++;
        }
      return $retorno;
  }
  public function getMenu(){
    $query = $this->con->query("SELECT * FROM menu");
      $retorno = [];
       $i=0;
       while ($fila = $query->fetch_assoc()){
        $retorno[$i] = $fila;
        $i++;
       }
     return $retorno;
 }
 public function getMenuVentas(){
  $query = $this->con->query("SELECT * FROM menu where acceso='A'");
    $retorno = [];
     $i=0;
     while ($fila = $query->fetch_assoc()){
      $retorno[$i] = $fila;
      $i++;
     }
   return $retorno;
}
  public function getAllUserData(){
    $query = $this->con->query("SELECT * FROM usuarios ");
    return $query;
  }

  public function getRegisterUser($nombre,$tipo,$usuario,$password,$imagenUsuario){
    $query = $this->con->query("INSERT INTO `usuarios` (`id_usu`, `login`, `tipo`, `nombre`, `password`, `foto`) 
    VALUES (NULL, '$usuario', '$tipo', '$nombre', '$password', '$imagenUsuario')");
    return $query;
  }

  public function deleteUser($idUsuario){
    $query = $this->con->query("DELETE FROM usuarios where id_usu=$idUsuario;");
    return $query;
  }
  public function updateUser ($login, $tipo, $nombre, $password, $foto, $idUsuario){
    $query = $this->con->query("UPDATE `usuarios` 
      SET `login` = '$login', 
          `tipo` = '$tipo', 
          `nombre` = '$nombre',
          `password` = '$password', 
          `foto` = '$foto' WHERE `usuarios`.`id_usu` = $idUsuario");

return $query;
  }
  public function getMensajeAlerta(){
    $query = $this->con->query("SELECT * FROM `alerta`");
    $retorno = [];
    $i = 0;
    while ($fila = $query->fetch_assoc()) {
        $retorno[$i] = $fila;
        $i++;
    }
    return $retorno;
  }

  public function  getDataFactura(){
    $query = $this->con->query("SELECT *FROM datos;");
    return $query;
  }
}
?>